package ru.t1.chubarov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.chubarov.tm")
public class ApplicationConfiguration {
}
