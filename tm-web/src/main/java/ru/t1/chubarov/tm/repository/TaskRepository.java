package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
