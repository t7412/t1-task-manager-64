package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.chubarov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "tsk_"+id.substring(1,4);;

    private String description;

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish = new Date();

    private String projectId;

    public Task(String name) {}

    public Task() {

    }

    public String getId() {
        return id;
    }
}
