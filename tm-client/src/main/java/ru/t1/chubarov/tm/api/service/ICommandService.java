package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    @Nullable
    Collection<AbstractListener> getTerminalCommands();

    @NotNull
    Iterable<AbstractListener> getCommandWithArgument();

}
