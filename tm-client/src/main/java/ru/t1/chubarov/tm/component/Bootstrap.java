package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.api.IServiceLocator;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.listener.AbstractListener;
import ru.t1.chubarov.tm.util.SystemUtil;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Nullable
    @Autowired
    private AbstractListener[] abstractCommands;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Nullable
    @Autowired
    private IAuthEndpoint authEndpoint;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void regestry(@Nullable final AbstractListener[] commands) {
        for (@Nullable final AbstractListener command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    @SneakyThrows
    public void start(@Nullable final String[] args) {
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        regestry(abstractCommands);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                prepareShutdown();
            }
        });
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

}
