package ru.t1.chubarov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.exception.AbstractException;

public class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
