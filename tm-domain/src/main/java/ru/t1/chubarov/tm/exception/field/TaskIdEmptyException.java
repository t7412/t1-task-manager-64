package ru.t1.chubarov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error. TaskId is empty.");
    }

}
