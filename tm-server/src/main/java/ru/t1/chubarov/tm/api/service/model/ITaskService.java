package ru.t1.chubarov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void add(@Nullable String userId, @Nullable Task model) throws Exception;

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull String name,
            @NotNull String description) throws Exception;

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<Task> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @NotNull
    Task findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId) throws Exception;

    void remove(@NotNull String userId, @Nullable Task model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    long getSize() throws Exception;

    long getSize(@Nullable String userId) throws Exception;

    void add(@NotNull Collection<Task> models) throws Exception;

    void set(@NotNull Collection<Task> models) throws Exception;

    void clear();

}
