package ru.t1.chubarov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {
    void add(@Nullable String userId, @Nullable Session model) throws Exception;

    @NotNull
    List<Session> findAll() throws Exception;

    @Nullable
    List<Session> findAll(@NotNull String userId) throws Exception;

    @NotNull
    Session findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@NotNull String userId, @Nullable Session model) throws Exception;

    void removeAll(@NotNull String userId, @Nullable Collection<Session> models) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    long getSize(@Nullable String userId) throws Exception;

    long getSize() throws Exception;
}
