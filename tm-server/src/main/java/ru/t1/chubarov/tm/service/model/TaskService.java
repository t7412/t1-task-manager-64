package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskModelRepository repository;

    @NotNull
    @Autowired
    private IUserModelRepository userRepository;

    public User findOneById(@Nullable final String userId) {
        return userRepository.findFirstById(userId);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        repository.saveAndFlush(task);
    }

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Task task = findOneById(userId, id);
        task.setStatus(status.toString());
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        repository.saveAndFlush(task);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.countAllByUserIdAndId(userId, id)>0;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        return repository.findAll();
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task model = repository.findFirstById(id);
        if (model == null) throw new TaskNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findAllByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @Nullable final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task model = new Task();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public long getSize() {
        return repository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countAllByUserId(userId);
    }

    @Override
    @Transactional
    public void add(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull final Task task : models) {
            repository.saveAndFlush(task);
        }
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        repository.deleteAll();
        for (@NotNull final Task task : models) {
            repository.saveAndFlush(task);
        }
    }

    @Override
    @Transactional
    public void clear() {
            repository.deleteAll();
    }

}